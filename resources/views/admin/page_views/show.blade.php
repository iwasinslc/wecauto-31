@extends('admin.layouts.app')
@section('title')
    {{ __('Transaction details') }}
@endsection
@section('breadcrumbs')
    <li><a href="{{route('admin.transactions.index')}}">{{ __('Page views data') }}</a></li>
    <li> {{ __('Page views data') }}</li>
@endsection
@section('content')
    <!-- row -->
    <div class="row">
        <!-- col -->
        <div class="col-md-12">

            <!-- tile -->
            <section class="tile">

                <!-- tile header -->
                <div class="tile-header dvd dvd-btm">
                    <h1 class="custom-font">{{ __('Page views data') }}</h1>
                    <ul class="controls">
                        <li>
                            <a role="button" class="tile-fullscreen">
                                <i class="fa fa-expand"></i> {{ __('Fullscreen') }}
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- /tile header -->

                <!-- tile body -->
                <div class="tile-body">
                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                            <tr>
                                <th scope="row">{{ __('User') }}</th>
                                <td>
                                    <a href="{{ route('admin.users.show',['user'=>$pageViews->user_id]) }}">{{ \App\Models\User::find($pageViews->user_id)->login }}</a>
                                </td>
                                <td><a href="mailto:{{ \App\Models\User::find($pageViews->user_id)->email }}">{{ \App\Models\User::find($pageViews->user_id)->email }}</a></td>
                            </tr>
                            <tr>
                                <th scope="row">{{ __('URL') }}</th>
                                <td colspan="2">
                                    <textarea class="form-control" style="max-width:100%; width:100%; min-height: 60px;" readonly>{!! !empty($pageViews->page_url) ? $pageViews->page_url : __('no data') !!}</textarea>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">{{ __('GET REQUEST') }}</th>
                                <td colspan="2">
                                    <?php
                                    if (!empty($pageViews->get_request)) {
                                        echo '<textarea class="form-control" style="max-width:100%; width:100%; min-height: 300px;" readonly>';
                                            print_r(json_decode($pageViews->get_request, true));
                                        echo '</textarea>';
                                        } else {
                                        echo __('no data');
                                    }
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">{{ __('POST REQUEST') }}</th>
                                <td colspan="2">
                                    <?php
                                    if (!empty($pageViews->post_request)) {
                                        echo '<textarea class="form-control" style="max-width:100%; width:100%; min-height: 300px;" readonly>';
                                            print_r(json_decode($pageViews->post_request, true));
                                        echo '</textarea>';
                                        } else {
                                        echo __('no data');
                                    }
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">{{ __('Date') }}</th>
                                <td>{{ $pageViews->created_at }}</td>
                            </tr>
                            </tbody>
                        </table>

                    </div>
                </div>
                <!-- /tile body -->

            </section>
            <!-- /tile -->

        </div>
        <!-- /col -->
    </div>
    <!-- /row -->

@endsection
