<?php
return [
    'order_created'       => '주문 #:id :amount :currency 이 성공적으로 작성되었습니다',
    'order_closed'        => '#:id :amount :currency 잔의 자금 부족로 주문이 마감되었습니다.',
    'sale'                => '판매 :amount :currency 은 잘 되었습니다',
    'purchase'            => '구매 :amount :currency 은 잘 되었습니다',
    'partner_accrue'      => ':login 사용자로부터 :level 단계의 :amount :currency 제휴 수수료를 방금 받았습니다.',
    'wallet_refiled'      => '당신의 지갑은 :amount 만큼 :currency 로 보충되었습니다.',
    'rejected_withdrawal' => ':amount :currency 금액에 대한 인출이 취소되었습니다.',
    'approved_withdrawal' => ':amount :currency 금액에 대한 인출이 확인되었습니다.',
    'new_partner'         => ':level 단계의 새로운 :login 파트너가 있습니다.',
    'parking_bonus'       => '보너스 parking :amount :currency',
    'licence_cash_back'   => '라이센스 구매를위한 캐쉬백 :amount :currency'
];