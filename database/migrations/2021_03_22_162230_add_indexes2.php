<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIndexes2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->index(['created_at', 'currency_id', 'type_id'], 'idx_transactions_created_currency_type');
            $table->index(['type_id','currency_id','approved','user_id'], 'idx_transactions_type_currency_user');
            $table->index(['user_id','type_id'], 'idx_transactions_user_type');
            $table->index(['created_at'], 'idx_transactions_created_at');
        });

        Schema::table('user_parents', function (Blueprint $table) {
            $table->index(['user_id','line'], 'idx_user_parents_user_line');
            $table->index(['parent_id','line'], 'idx_user_parents_parent_line');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->index(['id','created_at'], 'idx_user_created_at');
        });

        Schema::table('order_pieces', function (Blueprint $table) {
            $table->index(['user_id', 'main_currency_id', 'type'], 'idx_order_pieces_user_type_currency');
            $table->index(['currency_id', 'main_currency_id', 'created_at'], 'idx_order_pieces_currency_main_currency_date');
            $table->index(['currency_id', 'main_currency_id', 'user_id'], 'idx_order_pieces_currency_main_currency_user');
        });

        Schema::table('exchange_orders', function (Blueprint $table) {
            $table->index(['id', 'user_id', 'created_at'], 'idx_exchange_orders_order_user_date');
            $table->index(['rate', 'currency_id', 'main_currency_id', 'type', 'active'], 'idx_exchange_rate_currency_type');
            $table->index(['user_id', 'currency_id', 'main_currency_id'], 'idx_exchange_orders_order_user_date2');
            $table->index(['active', 'currency_id', 'main_currency_id', 'type', 'rate'], 'idx_exchange_orders_rate');
        });

        Schema::table('wallets', function (Blueprint $table) {
            $table->index(['user_id', 'currency_id', 'payment_system_id'], 'idx_wallets_user_currency_payment');
        });

        Schema::table('order_requests', function (Blueprint $table) {
            $table->index(['available_at', 'active'], 'idx_order_requests_available_active');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->dropIndex('idx_transactions_created_currency_type');
            $table->dropIndex('idx_transactions_type_currency_user');
            $table->dropIndex('idx_transactions_user_type');
            $table->dropIndex('idx_transactions_created_at');
        });

        Schema::table('user_parents', function (Blueprint $table) {
            $table->dropIndex('idx_user_parents_user_line');
            $table->dropIndex('idx_user_parents_parent_line');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->dropIndex('idx_user_created_at');
        });

        Schema::table('order_pieces', function (Blueprint $table) {
            $table->dropIndex('idx_order_pieces_user_type_currency');
            $table->dropIndex('idx_order_pieces_currency_main_currency_date');
            $table->dropIndex('idx_order_pieces_currency_main_currency_user');
        });
        Schema::table('exchange_orders', function (Blueprint $table) {
            $table->dropIndex('idx_exchange_orders_order_user_date');
            $table->dropIndex('idx_exchange_rate_currency_type');
            $table->dropIndex('idx_exchange_orders_order_user_date2');
            $table->dropIndex('idx_exchange_orders_rate');
        });

        Schema::table('wallets', function (Blueprint $table) {
            $table->dropIndex('idx_wallets_user_currency_payment');
        });

        Schema::table('order_requests', function (Blueprint $table) {
            $table->dropIndex('idx_order_requests_available_active');
        });

    }
}
